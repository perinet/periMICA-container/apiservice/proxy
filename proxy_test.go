/*
Copyright (c) 2018-2022 Perinet GmbH
All rights reserved

This software is dual-licensed: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 as
published by the Free Software Foundation. For the terms of this
license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
or contact us at https://perinet.io/contact/ when you want license
this software under a commercial license.
*/

package proxy

import (
	"bufio"
	"bytes"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/asn1"
	"encoding/json"
	"encoding/pem"
	"io"
	"log"
	"math/big"
	"net"
	"net/http"
	"os"
	"reflect"
	"testing"
	"time"

	"gitlab.com/perinet/generic/lib/httpserver"
	"gitlab.com/perinet/generic/lib/httpserver/auth"
	"gitlab.com/perinet/generic/lib/httpserver/rbac"
	"gitlab.com/perinet/generic/lib/utils/intHttp"
	"gitlab.com/perinet/generic/lib/utils/shellhelper"
	"gotest.tools/v3/assert"
)

func TestMain(m *testing.M) {
	auth.SetAuthMethod(auth.API_KEY)

	hostname, err := os.Hostname()
	if err != nil {
		log.Fatalln("Error getting hostname: ", err.Error())
	}

	hostCert, hostKey, err := createSelfSigned(hostname)
	if err != nil {
		log.Fatalln("error creating self signed certificate", err)
	}

	// TODO create proper root CA
	SetCertificates(httpserver.Certificates{
		CaCert:      hostCert,
		HostCert:    hostCert,
		HostCertKey: hostKey,
	})

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Hello World"))
	})

	// httpserver.AddPaths(PathsGet())

	m.Run()
}

func TestProxyInfo(t *testing.T) {
	var info ProxyInfo
	data := intHttp.Get(Proxy_Info_Get, nil)
	err := json.Unmarshal(data, &info)

	assert.NilError(t, err)
	assert.Assert(t, info.ApiVersion == "22")
}

func TestTcpProxy(t *testing.T) {
	go func() {
		listener, err := net.Listen("tcp", "[::1]:21880")
		assert.NilError(t, err)
		defer listener.Close()

		conn, err := listener.Accept()
		assert.NilError(t, err)

		reader := bufio.NewReader(conn)
		_, err = reader.ReadString('\n')
		assert.NilError(t, err)

		_, err = conn.Write([]byte("Hello World"))
		assert.NilError(t, err)
	}()

	proxyConfig := ProxyConfig{ExternalPort: 40880, ServiceAddress: "[::1]:21880", Role: rbac.NONE}
	proxyConfigJson, err := json.Marshal(proxyConfig)
	assert.NilError(t, err)

	resp := intHttp.Call(Proxy_Set, "PATCH", bytes.NewReader(proxyConfigJson), map[string]string{"service": "test"})
	assert.Assert(t, resp.StatusCode == http.StatusOK)

	time.Sleep(1 * time.Second)

	certificate, err := tls.X509KeyPair(certificates.HostCert, certificates.HostCertKey)
	assert.NilError(t, err)

	tlsConfig := tls.Config{Certificates: []tls.Certificate{certificate}, InsecureSkipVerify: true}

	clientConn, err := tls.Dial("tcp", "[::1]:40880", &tlsConfig)
	assert.NilError(t, err)

	_, err = clientConn.Write([]byte("Hello\n"))
	assert.NilError(t, err)

	reply := make([]byte, 1024)
	_, err = clientConn.Read(reply)
	assert.NilError(t, err)

	err = clientConn.Close()
	assert.NilError(t, err)

	data := string(bytes.Trim(reply, "\x00"))
	assert.Assert(t, data == "Hello World")

	time.Sleep(1 * time.Second)

	resp2 := intHttp.Call(Proxy_Delete, "DELETE", nil, map[string]string{"service": "test"})
	assert.Assert(t, resp2.StatusCode == http.StatusNoContent)
}

func TestHttpProxy(t *testing.T) {
	shellhelper.CommandCall = func(name string, arg ...string) shellhelper.CmdResult {
		if name == "cfgmgr" && reflect.DeepEqual(arg, []string{`.system.certificates.root`}) {
			return shellhelper.CmdResult{Stdout: `null`}
		} else {
			t.Log("error: unexpected command call, return exit code 255:", name, arg)
			return shellhelper.CmdResult{Exitcode: 255}
		}
	}
	defer func() { shellhelper.CommandCall = shellhelper.Default_CommandCall }()

	go func() {
		err := http.ListenAndServe("[::1]:11880", nil)
		assert.NilError(t, err)
	}()

	proxyConfig := ProxyHttpConfig{InsecureTLS: true, ServiceAddress: "http://[::1]:11880/", Role: rbac.USER}
	proxyConfigJson, err := json.Marshal(proxyConfig)
	assert.NilError(t, err)

	resp := intHttp.Call(Proxy_Http_Set, "PATCH", bytes.NewReader(proxyConfigJson), map[string]string{"service": "test2"})
	assert.Assert(t, resp.StatusCode == http.StatusNoContent)

	time.Sleep(1 * time.Second)

	// resp2 := httptest.NewRecorder()
	// req2, _ := http.NewRequest("GET", "", nil)
	// req2 = mux.SetURLVars(req2, map[string]string{"service": "test2"})
	// Proxy_Http_Handler(periHttp.New(resp2, req2, rbac.ADMIN))
	// assert.Assert(t, resp2.Code == http.StatusOK)

	// data2, err := io.ReadAll(resp2.Body)
	// assert.NilError(t, err)
	// assert.Assert(t, string(data2) == "Hello World")

	resp2 := intHttp.CallWithRole(Proxy_Http_Handler, "GET", nil, map[string]string{"service": "test2"}, rbac.ADMIN)
	assert.Assert(t, resp2.StatusCode == http.StatusOK)
	data2, err := io.ReadAll(resp2.Body)
	assert.NilError(t, err)
	assert.Assert(t, string(data2) == "Hello World")

	time.Sleep(1 * time.Second)

	resp3 := intHttp.Call(Proxy_Http_Delete, "DELETE", nil, map[string]string{"service": "test2"})
	assert.Assert(t, resp3.StatusCode == http.StatusNoContent)
}

func TestHttpsProxy(t *testing.T) {
	shellhelper.CommandCall = func(name string, arg ...string) shellhelper.CmdResult {
		if name == "cfgmgr" && reflect.DeepEqual(arg, []string{`.system.certificates.root`}) {
			return shellhelper.CmdResult{Stdout: `null`}
		} else {
			t.Log("error: unexpected command call, return exit code 255:", name, arg)
			return shellhelper.CmdResult{Exitcode: 255}
		}
	}
	defer func() { shellhelper.CommandCall = shellhelper.Default_CommandCall }()

	certificate, err := tls.X509KeyPair(certificates.HostCert, certificates.HostCertKey)
	assert.NilError(t, err)

	srv := &http.Server{
		Addr: "[::1]:11443",
		TLSConfig: &tls.Config{
			ClientAuth:   tls.NoClientCert,
			Certificates: []tls.Certificate{certificate},
		},
	}

	go func() {
		err := srv.ListenAndServeTLS("", "")
		assert.NilError(t, err)
	}()

	proxyConfig := ProxyHttpConfig{InsecureTLS: true, ServiceAddress: "https://[::1]:11443/", Role: rbac.USER}
	proxyConfigJson, err := json.Marshal(proxyConfig)
	assert.NilError(t, err)

	resp := intHttp.Call(Proxy_Http_Set, "PATCH", bytes.NewReader(proxyConfigJson), map[string]string{"service": "test3"})
	assert.Assert(t, resp.StatusCode == http.StatusNoContent)

	time.Sleep(1 * time.Second)

	resp2 := intHttp.CallWithRole(Proxy_Http_Handler, "GET", nil, map[string]string{"service": "test3"}, rbac.ADMIN)
	assert.Assert(t, resp2.StatusCode == http.StatusOK)
	data2, err := io.ReadAll(resp2.Body)
	assert.NilError(t, err)
	assert.Assert(t, string(data2) == "Hello World")

	time.Sleep(1 * time.Second)

	resp3 := intHttp.Call(Proxy_Http_Delete, "DELETE", nil, map[string]string{"service": "test3"})
	assert.Assert(t, resp3.StatusCode == http.StatusNoContent)
}

func createSelfSigned(hostname string) ([]byte, []byte, error) {
	priv, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	if err != nil {
		log.Println(err)
		return nil, nil, err
	}

	serialNumber, _ := rand.Int(rand.Reader, new(big.Int).Lsh(big.NewInt(1), 128))

	// Extension Subject Alternative Name - DNS should be included here
	extSubjectAltName := pkix.Extension{}
	extSubjectAltName.Id = asn1.ObjectIdentifier{2, 5, 29, 17} // OIA for SAN
	extSubjectAltName.Critical = false
	extSubjectAltName.Value = []byte(`DNS:` + hostname + `, DNS:` + hostname + `.local`)

	template := x509.Certificate{
		SerialNumber: serialNumber,
		DNSNames:     []string{hostname, hostname + ".local"},
		Subject: pkix.Name{
			CommonName: hostname + ".local",
		},
		Extensions: []pkix.Extension{extSubjectAltName},
		NotBefore:  time.Now(),
		NotAfter:   time.Now().Add(1825 * 24 * time.Hour),

		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		BasicConstraintsValid: true,
	}

	certOut := &bytes.Buffer{}

	derBytes, err := x509.CreateCertificate(rand.Reader, &template, &template, priv.Public(), priv)
	if err != nil {
		log.Printf("Failed to create certificate: %s", err)
		return nil, nil, err
	}
	pem.Encode(certOut, &pem.Block{Type: "CERTIFICATE", Bytes: derBytes})

	keyOut := &bytes.Buffer{}

	derKeyBytes, err := x509.MarshalECPrivateKey(priv)
	if err != nil {
		log.Printf("Unable to marshal ECDSA private key: %v", err)
		return nil, nil, err
	}
	pem.Encode(keyOut, &pem.Block{Type: "EC PRIVATE KEY", Bytes: derKeyBytes})

	return certOut.Bytes(), keyOut.Bytes(), nil
}
