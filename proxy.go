/*
Copyright (c) 2018-2022 Perinet GmbH
All rights reserved

This software is dual-licensed: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 as
published by the Free Software Foundation. For the terms of this
license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
or contact us at https://perinet.io/contact/ when you want license
this software under a commercial license.
*/

package proxy

import (
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"io"
	"log"
	"net"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strconv"
	"strings"

	"gitlab.com/perinet/generic/lib/httpserver"
	"gitlab.com/perinet/generic/lib/httpserver/auth"
	"gitlab.com/perinet/generic/lib/httpserver/periHttp"
	"gitlab.com/perinet/generic/lib/httpserver/rbac"
	"golang.org/x/exp/maps"
)

const (
	API_VERSION = "22"
)

type ProxyInfo struct {
	ApiVersion json.Number `json:"api_version"`
}

type ProxyConfig struct {
	ExternalPort   uint16    `json:"externalPort"`
	ServiceAddress string    `json:"serviceAddress"`
	Role           rbac.Role `json:"role"`
}

type ProxyHttpConfig struct {
	ServiceAddress string    `json:"serviceAddress"`
	Role           rbac.Role `json:"role"`
	InsecureTLS    bool      `json:"insecure_tls"`
}

var (
	certificates       httpserver.Certificates
	tcpListener        map[string]net.Listener          = make(map[string]net.Listener)
	httpServices       map[string]ProxyHttpConfig       = make(map[string]ProxyHttpConfig)
	httpReverseProxies map[string]httputil.ReverseProxy = make(map[string]httputil.ReverseProxy)
)

func SetCertificates(certs httpserver.Certificates) {
	certificates = certs
}

// URL endpoints definition
func PathsGet() []httpserver.PathInfo {
	return []httpserver.PathInfo{
		{Url: "/proxy", Method: httpserver.GET, Role: rbac.NONE, Call: Proxy_Info_Get},
		{Url: "/proxy", Method: httpserver.DELETE, Role: rbac.ADMIN, Call: Proxy_Delete_All},
		{Url: "/proxy/_tcp/{service}", Method: httpserver.PATCH, Role: rbac.ADMIN, Call: Proxy_Set},
		{Url: "/proxy/_tcp/{service}", Method: httpserver.DELETE, Role: rbac.ADMIN, Call: Proxy_Delete},
		{Url: "/proxy/_http/{service}", Method: httpserver.PATCH, Role: rbac.ADMIN, Call: Proxy_Http_Set},
		{Url: "/proxy/_http/{service}", Method: httpserver.DELETE, Role: rbac.ADMIN, Call: Proxy_Http_Delete},
		{Url: "/proxy/http/{service}/", UrlIsPrefix: true, Method: httpserver.GET, Role: rbac.NONE, Call: Proxy_Http_Handler},
		{Url: "/proxy/http/{service}/", UrlIsPrefix: true, Method: httpserver.HEAD, Role: rbac.NONE, Call: Proxy_Http_Handler},
		{Url: "/proxy/http/{service}/", UrlIsPrefix: true, Method: httpserver.POST, Role: rbac.NONE, Call: Proxy_Http_Handler},
		{Url: "/proxy/http/{service}/", UrlIsPrefix: true, Method: httpserver.PATCH, Role: rbac.NONE, Call: Proxy_Http_Handler},
		{Url: "/proxy/http/{service}/", UrlIsPrefix: true, Method: httpserver.PUT, Role: rbac.NONE, Call: Proxy_Http_Handler},
		{Url: "/proxy/http/{service}/", UrlIsPrefix: true, Method: httpserver.DELETE, Role: rbac.NONE, Call: Proxy_Http_Handler},
	}
}

// Proxy_Info_Get: returns info object
func Proxy_Info_Get(p periHttp.PeriHttp) {
	p.JsonResponse(http.StatusOK, ProxyInfo{ApiVersion: API_VERSION})
}

func Proxy_Delete_All(p periHttp.PeriHttp) {
	for service, service_listener := range tcpListener {
		service_listener.Close()
		delete(tcpListener, service)
	}

	p.EmptyResponse(http.StatusNoContent)
}

func Proxy_Delete(p periHttp.PeriHttp) {
	service := p.Vars()["service"]

	if service_listener, ok := tcpListener[service]; ok {
		service_listener.Close()
		delete(tcpListener, service)
		p.EmptyResponse(http.StatusNoContent)
		return
	}

	p.EmptyResponse(http.StatusNotFound)
}

func Proxy_Set(p periHttp.PeriHttp) {
	// log.Println("Proxy_Set")
	service := p.Vars()["service"]

	payload, _ := p.ReadBody()

	newProxyConfig := ProxyConfig{Role: rbac.INTERNAL}
	err := json.Unmarshal(payload, &newProxyConfig)
	if err != nil {
		p.JsonResponse(http.StatusInternalServerError, json.RawMessage(`{"error": "`+err.Error()+`"}`))
		return
	}

	certificate, err := tls.X509KeyPair(certificates.HostCert, certificates.HostCertKey)
	if err != nil {
		log.Printf("Proxy_Set LoadX509KeyPair(): %v", err)
		p.JsonResponse(http.StatusInternalServerError, json.RawMessage(`{"error": "`+err.Error()+`"}`))
		return
	}

	var config tls.Config
	if auth.GetAuthMethod() == auth.MTLS {
		config = tls.Config{
			ClientAuth:   tls.RequestClientCert,
			Certificates: []tls.Certificate{certificate},
			VerifyConnection: func(cs tls.ConnectionState) error {
				role, err := auth.VerifyCertificates(cs.PeerCertificates)
				if newProxyConfig.Role > role {
					log.Println(err)
					return err
				}
				return nil
			},
		}
	} else if auth.GetAuthMethod() == auth.NONE || (auth.GetAuthMethod() == auth.API_KEY && newProxyConfig.Role == rbac.NONE) {
		config = tls.Config{
			ClientAuth:   tls.NoClientCert,
			Certificates: []tls.Certificate{certificate},
		}
	} else {
		p.JsonResponse(http.StatusInternalServerError, json.RawMessage(`{"error": "auth method not supported for that role"}`))
		return
	}

	// close old service
	if service_listener, ok := tcpListener[service]; ok {
		service_listener.Close()
	}

	tcpListener[service], err = tls.Listen("tcp", ":"+strconv.FormatUint(uint64(newProxyConfig.ExternalPort), 10), &config)
	if err != nil {
		log.Println(err)
		p.JsonResponse(http.StatusInternalServerError, json.RawMessage(`{"error": "`+err.Error()+`"}`))
		return
	}

	p.EmptyResponse(http.StatusOK)

	go func() {

		for {
			conn, err := tcpListener[service].Accept()
			if err != nil {
				log.Println(err)
				break
			}

			handleRequest(conn, newProxyConfig.ServiceAddress)
		}

	}()
}

func Proxy_Http_Set(p periHttp.PeriHttp) {
	// log.Println("Proxy_Http_Set")
	service := p.Vars()["service"]

	payload, _ := p.ReadBody()

	newProxyConfig := ProxyHttpConfig{Role: rbac.INTERNAL}
	err := json.Unmarshal(payload, &newProxyConfig)
	if err != nil {
		p.JsonResponse(http.StatusInternalServerError, json.RawMessage(`{"error": "`+err.Error()+`"}`))
		return
	}

	httpServices[service] = newProxyConfig
	// log.Println("Proxy_Http_Set: ", service, httpServices[service])

	targetUrl, err := url.Parse(httpServices[service].ServiceAddress)
	if err != nil {
		log.Println(err)
	}

	// allow websocket prefix
	switch targetUrl.Scheme {
	case "ws":
		targetUrl.Scheme = "http"
	case "wss":
		targetUrl.Scheme = "https"
	}

	certpool := x509.NewCertPool()
	ok := certpool.AppendCertsFromPEM(certificates.CaCert)
	if !ok && targetUrl.Scheme == "https" {
		p.JsonResponse(http.StatusInternalServerError, json.RawMessage(`{"error": "failed to set root CA"}`))
		return
	}

	httpReverseProxies[service] = httputil.ReverseProxy{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: httpServices[service].InsecureTLS,
				RootCAs:            certpool,
			},
		},
		Rewrite: func(r *httputil.ProxyRequest) {
			_, url_suffix, _ := strings.Cut(r.In.URL.Path, "/proxy/http/"+service)

			r.SetURL(targetUrl)
			r.Out.Host = r.In.Host
			r.Out.URL.Path = url_suffix
		},
		ModifyResponse: func(r *http.Response) error {
			location := r.Header.Get("Location")
			if location != "" && strings.HasPrefix(location, "/") {
				location = "/proxy/http/" + service + location
				r.Header.Set("Location", location)
			}

			return nil
		},
	}

	p.EmptyResponse(http.StatusNoContent)
}

func Proxy_Http_Delete_All(p periHttp.PeriHttp) {
	maps.Clear(httpServices)

	p.EmptyResponse(http.StatusNoContent)
}

func Proxy_Http_Delete(p periHttp.PeriHttp) {
	service := p.Vars()["service"]

	if _, ok := httpServices[service]; ok {
		delete(httpServices, service)
		p.EmptyResponse(http.StatusNoContent)
		return
	}

	p.EmptyResponse(http.StatusNotFound)
}

func Proxy_Http_Handler(p periHttp.PeriHttp) {
	// log.Println("Proxy_Http_Handler: ", r.URL.Path)
	service_name := p.Vars()["service"]

	proxy, isRegistered := httpReverseProxies[service_name]
	if !isRegistered {
		p.EmptyResponse(http.StatusNotFound)
		return
	}

	// check role
	if !p.IsAuthorized(httpServices[service_name].Role) {
		p.JsonResponse(http.StatusUnauthorized, json.RawMessage(`{"error": "user not authorized"}`))
		return
	}

	proxy.ServeHTTP(p.Resp(), p.Req())
}

func handleRequest(conn net.Conn, serviceAddress string) {
	proxy, err := net.Dial("tcp", serviceAddress)
	if err != nil {
		log.Println(err)
		return
	}

	go copyIO(conn, proxy)
	go copyIO(proxy, conn)
}

func copyIO(src, dest net.Conn) {
	defer src.Close()
	defer dest.Close()
	io.Copy(src, dest)
}
